# Parkit Theme

This theme inherits from the global project theme.

This theme is meant for just parking a business domain. It is a super simple, minimal, 1 cover page.

This theme is not to be committed into project repo.

To use this theme change the gitlab var THEME to parkit.
